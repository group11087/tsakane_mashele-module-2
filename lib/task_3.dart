void main() {
  List<WinningApp> winningapp = [
    WinningApp(
      'Naked Insurance',
      'Finance',
      'Simnikiwe mzekandaba and Lebone Mano',
      '2019',
    )
  ];
  print(winningapp[0].name);
  print(winningapp[0].category);
  print(winningapp[0].year);
  print(winningapp[0].developer);
  print(winningapp[0].capitalizedName);
}

class WinningApp {
  String? name;
  String? category;
  String? developer;
  String? year;

  WinningApp(this.name, this.category, this.developer, this.year);

  String get capitalizedName {
    return name?.toUpperCase() ?? '';
  }
}
